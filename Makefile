gen:
	protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative ./chatservice.proto

save:
	git add *
	git commit -m 'AUTO: updated proto file'
	git push
